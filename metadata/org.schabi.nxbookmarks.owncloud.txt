Categories:Multimedia,Internet
License:MIT
Web Site:https://github.com/theScrabi/OCBookmarks
Source Code:https://github.com/theScrabi/OCBookmarks
Issue Tracker:https://github.com/theScrabi/OCBookmarks/issues
Changelog:https://github.com/theScrabi/OCBookmarks/releases

Auto Name:OwnCloud Bookmarks
Summary:A front end for the Nextcloud Bookmark app
Description:
An Android front end for the Nextcloud/Owncloud Bookmark App based on the new
REST API that was introduced by Bookmarks version 0.10.2 With this app you can
add/edit/delete and view bookmarks, and sync them with your ownCloud.

However you need to have the Bookmarks app in minimal required version 0.10.2
installed and enabled on you ownCloud.

If you need more information about the Nextcloud Bookmark app, you can follow
this link: [https://marketplace.owncloud.com/apps/bookmarks]
.

Repo Type:git
Repo:https://github.com/theScrabi/OCBookmarks

Build:1.1,2
    commit=v1.1
    subdir=app
    output=build/outputs/apk/*.apk
    build=gradle assembleOwncloud

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:1.1
Current Version Code:1
